using System;

namespace ConsoleApp
{
    class Conflict
    {
        static void Main(string[] args)
        {
            SayHello("Mateusz", "Mulet", " Male");
        }

        static void SayHello(string name, string surname, string sex)
        {
            Console.WriteLine("Dzie� dobry " + name + surname + sex);
        }
    }
}